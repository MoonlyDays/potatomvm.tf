//============= Copyright 2021 =================//
//
//===============================================//

const CREATORS_PROVIDER_ID = 5;

const CServerGroups = [{
    dom_id: "serverlist_archive",
    hostname_anchor: "MvM Archive"
}, {
    dom_id: "serverlist_memes",
    hostname_anchor: "MvM Memes Archive"
}];

function Servers_LoadServerInfo() {

    // Getting URL for the request.
    let sURL = `https://creators.tf/api/IServers/GServerList?provider=${CREATORS_PROVIDER_ID}`;

    // Making this request.
    fetch(sURL)
        // Response should be treated as JSON.
        .then(r => r.json())
        .then((r) => {

            for (let xIndicator of document.querySelectorAll(".loading_indicator")) {
                xIndicator.remove();
            }

            // Let's loop through each group and see what servers we want to put.
            for (let Group of CServerGroups) {

                // Find parent element to put server elements in.
                let xElement = document.querySelector(`#${Group.dom_id}`);

                // If it was found...
                if (xElement) {

                    // We will be adding new html on top of what's already there.
                    let sHTML = xElement.innerHTML;

                    // Find data for it in the request response.
                    let xServers = r.servers.filter(s => s.hostname.includes(Group.hostname_anchor));

                    for (let xData of xServers) {
                        // We skip it, if it's offline.
                        if (xData.is_down) continue;

                        //========================================//
                        // Wave Status
                        //========================================//
                        let sWaveMatch = xData.game.match(/\(Wave ([0-9]+)\/([0-9]+) :: (.*)\)/) || [];

                        // Wave Number
                        let iCurrentWave = "~";
                        let iMaxWave = "~";
                        let sWaveStatus = "~";

                        if (sWaveMatch[1]) iCurrentWave = sWaveMatch[1];
                        if (sWaveMatch[2]) iMaxWave = sWaveMatch[2];
                        if (sWaveMatch[3]) sWaveStatus = sWaveMatch[3];

                        //========================================//
                        // Players
                        //========================================//

                        let sPlayerColor = "#fff";
                        if (xData.online == 0) {
                            sPlayerColor = "var(--color-red)";
                        } else if (xData.online < xData.maxplayers) {
                            sPlayerColor = "var(--color-yellow)";
                        } else {
                            sPlayerColor = "var(--color-green)";
                        }

                        //========================================//
                        // Map Name, Mission Name and Difficulty
                        //========================================//

                        let sMapMatch = xData.map.match(/mvm\_([a-z0-9_]+)\_(exp|adv|int|1)\_([a-z0-9_]+)/) || [];

                        // MAP NAME
                        let sMap = xData.map;

                        // If we have information for the map name.
                        if (sMapMatch[1]) {
                            // Strip version number.
                            sMap = sMapMatch[1].replace(/_[a|b|rc|final]+[0-9]+[a-z]*/, '');

                            // Uppercase every first letter in every word and replace _ with whitespaces.
                            // I.e. map_name => Map Name.


                            sMap = sMap.split("_").map(w => {
                                return (w[0] || "").toUpperCase() + w.substr(1);
                            }).join(" ");

                        }

                        // MISSION NAME
                        let sMission = "~";

                        // If we have information for the mission name.
                        if (sMapMatch[3]) {
                            // Uppercase every first letter in every word and replace _ with whitespaces.
                            // I.e. map_name => Map Name.

                            sMission = sMapMatch[3].split("_").map(w => {
                                return (w[0] || "").toUpperCase() + w.substr(1);
                            }).join(" ");
                        }

                        // DIFFICULTY
                        let sDifficulty = "~";
                        let sDifficultyColor = "#fff";

                        if (sMapMatch[2]) {
                            switch (sMapMatch[2]) {

                                // Intermediate (Green)
                                case "int":
                                    sDifficulty = "Intermediate";
                                    sDifficultyColor = "var(--color-green)";
                                    break;

                                    // Advanced (Yellow)
                                case "adv":
                                    sDifficulty = "Advanced";
                                    sDifficultyColor = "var(--color-yellow)";
                                    break;

                                    // Expert (Red)
                                case "exp":
                                    sDifficulty = "Expert";
                                    sDifficultyColor = "var(--color-red)";
                                    break;
                                    break;

                                    // Memes (Purple)
                                case "1":
                                    sDifficulty = "Memes";
                                    sDifficultyColor = "var(--color-purple)";
                                    break;
                            }
                        }

                        //=====================================//
                        // Final Compilation.
                        //=====================================//

                        let sColumns = [
                            // Server Index
                            xData.id,
                            // Region
                            `<img src="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.4.3/flags/4x3/${xData.region}.svg" alt="${xData.region.toUpperCase()}"> ${xData.region.toUpperCase()}`,
                            // Map Name
                            sMap,
                            // Mission Name
                            sMission,
                            // Difficulty
                            `<span style="color: ${sDifficultyColor}">${sDifficulty}</span>`,
                            // Wave Counter
                            `${iCurrentWave} / ${iMaxWave}`,
                            // Wave Status
                            sWaveStatus,
                            // Online
                            `<span style="color: ${sPlayerColor}">${xData.online} / ${xData.maxplayers}</span>`,
                            // Connect Button
                            `<a href="steam://connect/${xData.ip}:${xData.port}"><div class="btn_connect">Join</div></a>`
                        ];

                        sHTML += `<tr>${sColumns.map(c => `<td>${c}</td>`).join("")}</tr>`;
                    }
                    xElement.innerHTML = sHTML;
                }
            }
        });
}

Servers_LoadServerInfo();
